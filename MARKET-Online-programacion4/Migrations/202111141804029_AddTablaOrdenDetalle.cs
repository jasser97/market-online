﻿namespace MARKET_Online_programacion4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablaOrdenDetalle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrdenDetalles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrdenId = c.String(nullable: false, maxLength: 10),
                        ProductoId = c.String(nullable: false, maxLength: 10),
                        Precio = c.String(nullable: false, maxLength: 10),
                        Cantidad = c.String(nullable: false, maxLength: 20),
                        Descuento = c.String(nullable: false, maxLength: 20),
                        Total = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.OrdenDetalles");
        }
    }
}
