﻿namespace MARKET_Online_programacion4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablaOrden : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ordens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaCreacion = c.DateTime(nullable: false),
                        ClienteId = c.String(nullable: false, maxLength: 10),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Ordens");
        }
    }
}
