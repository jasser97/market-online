﻿namespace MARKET_Online_programacion4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablaClasificacionProducto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClasificacionProductoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false, maxLength: 10),
                        Clasificacion = c.String(nullable: false, maxLength: 30),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Codigo, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ClasificacionProductoes", new[] { "Codigo" });
            DropTable("dbo.ClasificacionProductoes");
        }
    }
}
