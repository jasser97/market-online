namespace MARKET_Online_programacion4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablaUsuario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Iduser = c.Int(nullable: false, identity: true),
                        NombreUser = c.String(nullable: false, maxLength: 75),
                        Contraseña = c.String(nullable: false, maxLength: 15),
                        Correo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Iduser);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Usuarios");
        }
    }
}
