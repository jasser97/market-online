namespace MARKET_Online_programacion4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreacionTablas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClasificacionProducto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false, maxLength: 10),
                        Clasificacion = c.String(nullable: false, maxLength: 30),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Codigo, unique: true);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Detalle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PrecioM = c.Double(nullable: false),
                        Cantidad = c.Double(nullable: false),
                        Descuento = c.Double(nullable: false),
                        Total = c.Double(nullable: false),
                        Pedidoid = c.Int(nullable: false),
                        Productoid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pedido", t => t.Pedidoid)
                .ForeignKey("dbo.Producto", t => t.Productoid)
                .Index(t => t.Pedidoid)
                .Index(t => t.Productoid);
            
            CreateTable(
                "dbo.Pedido",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FechaCreacion = c.DateTime(nullable: false),
                        Clienteid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cliente", t => t.Clienteid)
                .Index(t => t.Clienteid);
            
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(maxLength: 15),
                        Nombre = c.String(nullable: false, maxLength: 75),
                        Estado = c.Boolean(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        UnidadMedidaId = c.Int(nullable: false),
                        ClasificacionProductoId = c.Int(nullable: false),
                        Precio = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClasificacionProducto", t => t.ClasificacionProductoId)
                .ForeignKey("dbo.UnidadMedida", t => t.UnidadMedidaId)
                .Index(t => t.Codigo, unique: true)
                .Index(t => t.UnidadMedidaId)
                .Index(t => t.ClasificacionProductoId);
            
            CreateTable(
                "dbo.UnidadMedida",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false, maxLength: 10),
                        Descripcion = c.String(nullable: false, maxLength: 30),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        Iduser = c.Int(nullable: false, identity: true),
                        NombreUser = c.String(nullable: false, maxLength: 75),
                        Contraseña = c.String(nullable: false, maxLength: 15),
                        Correo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Iduser);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Detalle", "Productoid", "dbo.Producto");
            DropForeignKey("dbo.Producto", "UnidadMedidaId", "dbo.UnidadMedida");
            DropForeignKey("dbo.Producto", "ClasificacionProductoId", "dbo.ClasificacionProducto");
            DropForeignKey("dbo.Detalle", "Pedidoid", "dbo.Pedido");
            DropForeignKey("dbo.Pedido", "Clienteid", "dbo.Cliente");
            DropIndex("dbo.Producto", new[] { "ClasificacionProductoId" });
            DropIndex("dbo.Producto", new[] { "UnidadMedidaId" });
            DropIndex("dbo.Producto", new[] { "Codigo" });
            DropIndex("dbo.Pedido", new[] { "Clienteid" });
            DropIndex("dbo.Detalle", new[] { "Productoid" });
            DropIndex("dbo.Detalle", new[] { "Pedidoid" });
            DropIndex("dbo.ClasificacionProducto", new[] { "Codigo" });
            DropTable("dbo.Usuario");
            DropTable("dbo.UnidadMedida");
            DropTable("dbo.Producto");
            DropTable("dbo.Pedido");
            DropTable("dbo.Detalle");
            DropTable("dbo.Cliente");
            DropTable("dbo.ClasificacionProducto");
        }
    }
}
