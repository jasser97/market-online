﻿namespace MARKET_Online_programacion4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablaProducto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(maxLength: 15),
                        Nombre = c.String(nullable: false, maxLength: 75),
                        Estado = c.Boolean(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        UnidadMedidaId = c.Int(nullable: false),
                        ClasificacionProductoId = c.Int(nullable: false),
                        Precio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClasificacionProductoes", t => t.ClasificacionProductoId)
                .ForeignKey("dbo.UnidadMedidas", t => t.UnidadMedidaId)
                .Index(t => t.Codigo, unique: true)
                .Index(t => t.UnidadMedidaId)
                .Index(t => t.ClasificacionProductoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productoes", "UnidadMedidaId", "dbo.UnidadMedidas");
            DropForeignKey("dbo.Productoes", "ClasificacionProductoId", "dbo.ClasificacionProductoes");
            DropIndex("dbo.Productoes", new[] { "ClasificacionProductoId" });
            DropIndex("dbo.Productoes", new[] { "UnidadMedidaId" });
            DropIndex("dbo.Productoes", new[] { "Codigo" });
            DropTable("dbo.Productoes");
        }
    }
}
