﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MARKET_Online_programacion4.DataBase;
using MARKET_Online_programacion4.Models;

namespace MARKET_Online_programacion4.Controllers
{
    public class ClasificacionProductoController : Controller
    {
        private MarketContext db = new MarketContext();

        // GET: ClasificacionProducto
        public ActionResult Index()
        {
            return View(db.ClasificacionProductos.ToList());
        }

        // GET: ClasificacionProducto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClasificacionProducto clasificacionProducto = db.ClasificacionProductos.Find(id);
            if (clasificacionProducto == null)
            {
                return HttpNotFound();
            }
            return View(clasificacionProducto);
        }

        // GET: ClasificacionProducto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClasificacionProducto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Codigo,Clasificacion,Estado")] ClasificacionProducto clasificacionProducto)
        {
            if (ModelState.IsValid)
            {
                db.ClasificacionProductos.Add(clasificacionProducto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(clasificacionProducto);
        }

        // GET: ClasificacionProducto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClasificacionProducto clasificacionProducto = db.ClasificacionProductos.Find(id);
            if (clasificacionProducto == null)
            {
                return HttpNotFound();
            }
            return View(clasificacionProducto);
        }

        // POST: ClasificacionProducto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Codigo,Clasificacion,Estado")] ClasificacionProducto clasificacionProducto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clasificacionProducto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(clasificacionProducto);
        }

        // GET: ClasificacionProducto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClasificacionProducto clasificacionProducto = db.ClasificacionProductos.Find(id);
            if (clasificacionProducto == null)
            {
                return HttpNotFound();
            }
            return View(clasificacionProducto);
        }

        // POST: ClasificacionProducto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClasificacionProducto clasificacionProducto = db.ClasificacionProductos.Find(id);
            db.ClasificacionProductos.Remove(clasificacionProducto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
