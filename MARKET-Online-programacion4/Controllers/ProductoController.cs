﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MARKET_Online_programacion4.DataBase;
using MARKET_Online_programacion4.Models;
using MARKET_Online_programacion4.ViewsModel;

namespace MARKET_Online_programacion4.Controllers
{
    public class ProductoController : Controller
    {
        private MarketContext db = new MarketContext();

        // GET: Producto
        public ActionResult Index()
        {
            var productos = db.Productos.Include(p => p.ClasificacionProducto).Include(p => p.UnidadMedida);
            return View(productos.ToList());
        }

        // GET: Producto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Producto/Create
        public ActionResult Create()
        {
            var producto = new Producto();
            var unidadmedidas = db.UnidadMedidas.ToList();
            var clasificacionproducto = db.ClasificacionProductos.ToList();
            var ViewModel = new ProductoFormViewModel
            {
                Producto = producto,
                UnidadMedidas= unidadmedidas ,
                ClasificacionProductos=  clasificacionproducto
            };
            ViewBag.ClasificacionProductoId = new SelectList(db.ClasificacionProductos, "Id", "Codigo");
            ViewBag.UnidadMedidaId = new SelectList(db.UnidadMedidas, "Id", "Codigo");

            return View("ProductoForm", ViewModel);
            
            
        }

        // POST: Producto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Codigo,Nombre,Estado,FechaCreacion,UnidadMedidaId,ClasificacionProductoId,Precio")] Producto producto)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Productos.Add(producto);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.ClasificacionProductoId = new SelectList(db.ClasificacionProductos, "Id", "Codigo", producto.ClasificacionProductoId);
        //    ViewBag.UnidadMedidaId = new SelectList(db.UnidadMedidas, "Id", "Codigo", producto.UnidadMedidaId);
        //    return View(producto);
        //}

        // GET: Producto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            var unidadmedidas = db.UnidadMedidas.ToList();
            var clasificacionproducto = db.ClasificacionProductos.ToList();
            var ViewModel = new ProductoFormViewModel
            {
                Producto = producto,
                UnidadMedidas = unidadmedidas, 
                ClasificacionProductos= clasificacionproducto
            };

            ViewBag.ClasificacionProductoId = new SelectList(db.ClasificacionProductos, "Id", "Codigo", producto.ClasificacionProductoId);
            ViewBag.UnidadMedidaId = new SelectList(db.UnidadMedidas, "Id", "Codigo", producto.UnidadMedidaId);

            return View("ProductoForm", ViewModel);

        }

        // POST: Producto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Codigo,Nombre,Estado,FechaCreacion,UnidadMedidaId,ClasificacionProductoId,Precio")] Producto producto)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(producto).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ClasificacionProductoId = new SelectList(db.ClasificacionProductos, "Id", "Codigo", producto.ClasificacionProductoId);
        //    ViewBag.UnidadMedidaId = new SelectList(db.UnidadMedidas, "Id", "Codigo", producto.UnidadMedidaId);
        //    return View(producto);
        //}

        // GET: Producto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Producto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Producto producto = db.Productos.Find(id);
            db.Productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Guardar( Producto producto)
        {
           
            if (ModelState.IsValid)
            {
                if (producto.Id == 0)
                {
                    db.Productos.Add(producto);
                }
                else
                {
                    db.Entry(producto).State = EntityState.Modified;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            //ViewBag.ClasificacionProductoId = new SelectList(db.ClasificacionProductos, "Id", "Codigo", producto.ClasificacionProductoId);
            //ViewBag.UnidadMedidaId = new SelectList(db.UnidadMedidas, "Id", "Codigo", producto.UnidadMedidaId);
            return View(producto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
