﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MARKET_Online_programacion4.DataBase;
using MARKET_Online_programacion4.Models;
using MARKET_Online_programacion4.ViewsModel;

namespace MARKET_Online_programacion4.Controllers
{
    public class PedidoController : Controller
    {
        private MarketContext db = new MarketContext();

        // GET: Pedido
        public ActionResult Index()
        {
            var pedidos = db.Pedidos.Include(p => p.Cliente);
            return View(pedidos.ToList());
        }

        // GET: Pedido/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            return View(pedido);
        }

        // GET: Pedido/Create
        public ActionResult Create()
        {
            var clientes = db.Clientes.ToList();
            var pedidoView = new PedidoFormViewModel
            {
                Pedido = new Pedido(),
                Clientes = clientes,
                Producto = new List<DetalleFormViewModel>()
            };
            Session["pedidoView"] = pedidoView;
            return View("PedidoForm", pedidoView);

            //ViewBag.Clienteid = new SelectList(db.Clientes, "Id", "Nombre");

        }

        [HttpGet]
        public ActionResult Agregar()
        {
            var productos = db.Productos.ToList();
            var viewProductoOrden = new DetalleFormViewModel
            {
              
                Productos= productos,

            };

            return View("Agregar", viewProductoOrden);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Agregar(DetalleFormViewModel viewProductoOrden)
        {
            var pedidoView = Session["pedidoView"] as PedidoFormViewModel;
            var cantidad = viewProductoOrden.Detalle.Cantidad;
            var ProductoInDb = db.Productos.Find(viewProductoOrden.Detalle.Productoid);
            var detalle = new Detalle
            {
                Productoid = ProductoInDb.Id,
                PrecioM = ProductoInDb.Precio,
                Cantidad = cantidad, 
                Descuento= 0,
                Total= ProductoInDb.Precio * cantidad,
                Producto= ProductoInDb


            };

            viewProductoOrden = new DetalleFormViewModel
            {
                Detalle = detalle,
            };

            pedidoView.Producto.Add(viewProductoOrden);
            return View("PedidoForm", pedidoView);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Guardar(PedidoFormViewModel pedidoView)
        {
            pedidoView.Producto = (Session["pedidoView"] as PedidoFormViewModel).Producto;
            Pedido pedido = pedidoView.Pedido;

            db.Pedidos.Add(pedido);
            db.SaveChanges();

            var PedidoId = db.Pedidos.ToList().Select(p => p.Id).Max();

            foreach (var producto in pedidoView.Producto)
            {
                var detalle = new Detalle
                {
                    Pedidoid = PedidoId,
                    Productoid = producto.Detalle.Producto.Id,
                    PrecioM = producto.Detalle.Producto.Precio,
                    Cantidad = producto.Detalle.Cantidad,
                    Total = producto.Detalle.Total,
                    Descuento = producto.Detalle.Descuento,
                };

                db.Detalles.Add(detalle);
                db.SaveChanges();
            }

            var cliente = db.Clientes.ToList();
            var pedidoViewNuevo = new PedidoFormViewModel
            {
                Pedido = new Pedido(),
                Clientes = cliente,
                Producto = new List<DetalleFormViewModel>()
            };

            Session["pedidoView"] = pedidoViewNuevo;
            return View("PedidoForm", pedidoViewNuevo);

        }

        


            // POST: Pedido/Create
            // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
            // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
            [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FechaCreacion,Clienteid")] Pedido pedido)
        {
            if (ModelState.IsValid)
            {
                db.Pedidos.Add(pedido);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Clienteid = new SelectList(db.Clientes, "Id", "Nombre", pedido.Clienteid);
            return View(pedido);
        }

        // GET: Pedido/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            ViewBag.Clienteid = new SelectList(db.Clientes, "Id", "Nombre", pedido.Clienteid);
            return View(pedido);
        }

        // POST: Pedido/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FechaCreacion,Clienteid")] Pedido pedido)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pedido).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Clienteid = new SelectList(db.Clientes, "Id", "Nombre", pedido.Clienteid);
            return View(pedido);
        }

        // GET: Pedido/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            return View(pedido);
        }

        // POST: Pedido/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pedido pedido = db.Pedidos.Find(id);
            db.Pedidos.Remove(pedido);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
