﻿using MARKET_Online_programacion4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.ViewsModel
{
    public class DetalleFormViewModel
    {
        public  Detalle  Detalle { get; set; }

        public IEnumerable<Producto> Productos { get; set; }
    }
}