﻿using MARKET_Online_programacion4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.ViewsModel
{
    public class PedidoFormViewModel
    {
        public Pedido Pedido { get; set; }

        public  IEnumerable<Cliente> Clientes { get; set; }

        public DetalleFormViewModel Tabla { get; set; }

        public List<DetalleFormViewModel> Producto { get; set; }

    }
}