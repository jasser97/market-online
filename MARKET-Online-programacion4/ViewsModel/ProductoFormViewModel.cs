﻿using MARKET_Online_programacion4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.ViewsModel
{
    public class ProductoFormViewModel
    {
        public  Producto Producto  { get; set; }

        public IEnumerable<UnidadMedida> UnidadMedidas { get; set; }

        public  IEnumerable<ClasificacionProducto> ClasificacionProductos  { get; set; }
    }
}