﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class Pedido
    {
        [Key]
        public int Id { get; set; }

        
        public DateTime FechaCreacion { get; set; }

        public int Clienteid { get; set; }

        [ForeignKey("Clienteid")]
        public Cliente Cliente { get; set; }

        public ICollection<Detalle> Detalles  { get; set; }

    }
}