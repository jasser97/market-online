﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class Usuario
    {
        [Key]
        public int Iduser { get; set; }

        [Required]
        [StringLength(75)]
        public  string NombreUser { get; set; }

        [Required]
        [StringLength(15)]
        public string Contraseña { get; set; }

        [Required]
        public  string Correo { get; set; }


    }
}