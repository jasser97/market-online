﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class Orden
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime FechaCreacion { get; set; }
        [Required]
        [MaxLength(10)]
        public string ClienteId { get; set; }
    }
}