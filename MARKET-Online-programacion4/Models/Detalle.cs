﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class Detalle
    {
        [Key]
        public int Id { get; set; }

        
        public double PrecioM { get; set; } // era string y lo cambie a double

        
        public double Cantidad { get; set; } // era string y lo cambie a double

        
        public double Descuento { get; set; } // era string y lo cambie a double

       
        public double Total { get; set; } // era string y lo cambie a double

        public int Pedidoid { get; set; }
        [ForeignKey("Pedidoid")]

        public Pedido Pedido{ get; set; }

        public int Productoid { get; set; }
        [ForeignKey("Productoid")]
        public Producto Producto { get; set; }


    }
}