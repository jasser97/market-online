﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class OrdenDetalle
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(10)]
        public string OrdenId { get; set; }

        [Required]
        [MaxLength(10)]
        public string ProductoId { get; set; }

        [Required]
        [MaxLength(10)]
        public string Precio { get; set; }

        [Required]
        [MaxLength(20)]
        public string Cantidad { get; set; }

        [Required]
        [MaxLength(20)]
        public string Descuento { get; set; }


        [Required]
        [MaxLength(20)]
        public string Total { get; set; }
    }
}