﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class Producto
    {
        public int Id { get; set; }

        [Index(IsUnique = true)]
        [StringLength(15)]

        public string Codigo { get; set; }
        [Required]
        [StringLength(75)]

        public string Nombre { get; set; }
        [Required]

        public bool Estado { get; set; }
        [Required]

        public DateTime FechaCreacion { get; set; }

        public int UnidadMedidaId { get; set; }

        public int ClasificacionProductoId { get; set; }

        public double Precio { get; set; } // era int y lo puse double

        [ForeignKey("UnidadMedidaId")]
        public UnidadMedida UnidadMedida { get; set; }

        [ForeignKey("ClasificacionProductoId")]
        public ClasificacionProducto ClasificacionProducto { get; set; }


    }
}