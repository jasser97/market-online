﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class UnidadMedida
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(10)]
        public string Codigo { get; set; }
        [Required]
        [MaxLength(30)]
        public string Descripcion { get; set; }
        [Required]
        public bool Estado { get; set; }

    }
}