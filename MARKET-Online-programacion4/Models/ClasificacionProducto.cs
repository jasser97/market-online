﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.Models
{
    public class ClasificacionProducto
    {

        //[Key]
        public int Id { get; set; }
        [Required]
        [Index(IsUnique= true)]
        [MaxLength(10)]
        public string Codigo { get; set; }
        [Required]
        [MaxLength(30)]
        public string Clasificacion { get; set; }
        [Required]
        public bool Estado { get; set; }

    }
}