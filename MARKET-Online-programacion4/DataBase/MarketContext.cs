﻿using MARKET_Online_programacion4.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace MARKET_Online_programacion4.DataBase
{
    public class MarketContext: DbContext
    {
        public MarketContext():base ("Market")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }

        public DbSet <UnidadMedida> UnidadMedidas { get; set; }
        public DbSet<ClasificacionProducto> ClasificacionProductos { get; set; }
        public DbSet<Producto> Productos { get; set; }
        
        public DbSet<Cliente> Clientes { get; set; }
       
        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Detalle>  Detalles { get; set; }

        public DbSet<Pedido> Pedidos { get; set; }


    }
}